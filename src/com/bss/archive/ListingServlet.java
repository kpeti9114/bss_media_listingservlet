package com.bss.archive;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ListingServlet")
public class ListingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    public ListingServlet() {
        super();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        
        PrintWriter out = response.getWriter();
        
        URL url = new URL("");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            out.println(line);
        }
        bufferedReader.close();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
    
}
